#include "user_interface.h"
#include <iomanip>
#include <sstream>
#include <type_traits>
#include <unistd.h>

UI::UI(std::shared_ptr<Board> board, size_t dimension_x, size_t dimension_y)
    : board_(board), dimension_x_(dimension_x), dimension_y_(dimension_y) {
  // initialize screen
  initscr();
  // disable line buffering and character erasing
  cbreak();
  // enable colors
  start_color();
}

UI::~UI() { endwin(); }

void UI::setKeyEventCallback(EventCallback callback) {
  event_callback_ = callback;
}

bool UI::terminalSizeSufficient() {
  size_t total_width = info_and_legend_width + dimension_x_ +
                       2; // +2 are the borders for the game */

  // decide which height is bigger game_window or info+legend
  size_t total_height = 0;
  if ((info_height + legend_height) > (dimension_y_ + 2))
    total_height = info_height + legend_height;
  else
    total_height = dimension_y_ + 2; // again +2 for the borders of game

  if (static_cast<size_t>(LINES) < total_height)
    return false;
  else if (static_cast<size_t>(COLS) < total_width)
    return false;
  return true;
}

void UI::up() {
  if (cursor_y_ > 1)
    cursor_y_--;
}

void UI::down() {
  if (cursor_y_ < dimension_y_)
    cursor_y_++;
}

void UI::left() {
  if (cursor_x_ > 1)
    cursor_x_--;
}

void UI::right() {
  if (cursor_x_ < dimension_x_)
    cursor_x_++;
}

void UI::setColorScheme(UI::COLORSCHEMES scheme) {
  wbkgd(game_window_, COLOR_PAIR(scheme));
}

void UI::run() {
  // initialise ui
  initialize();

  // keep running until termination required
  while (!terminate_) {
    // first redraw info
    drawInfo();
    // draw the game board
    drawBoard();
    // move cursor back to user position
    wmove(game_window_, cursor_y_, cursor_x_);
    // wait for key event
    char key = wgetch(game_window_);
    // pass received key to handling function
    if (event_callback_)
      event_callback_(key, cursor_x_ - 1, cursor_y_ - 1);
    else // if no event_callback has been set -> exit
      terminate_ = true;
  }
}

void UI::terminate() { terminate_ = true; }

void UI::setColor(Board::FIELDTYPE type) {
  switch (type) {
  case Board::MINE:
    wattron(game_window_, COLOR_PAIR(COLORSCHEMES::MINE));
    break;
  case Board::INDICATOR:
    wattron(game_window_, COLOR_PAIR(COLORSCHEMES::INDICATOR));
    break;
  case Board::BLANK:
    wattron(game_window_, COLOR_PAIR(COLORSCHEMES::INDICATOR));
    break;
  case Board::COVERED:
    wattron(game_window_, COLOR_PAIR(COLORSCHEMES::COVERED));
    break;
  case Board::FLAG:
    wattron(game_window_, COLOR_PAIR(COLORSCHEMES::FLAG));
    break;
  }
}

char UI::typeToSymbol(Board::FIELDTYPE type) {
  switch (type) {
  case Board::MINE:
    return '*';
    break;
  case Board::INDICATOR:
    return '0';
    break;
  case Board::BLANK:
    return '.';
    break;
  case Board::COVERED:
    return '#';
    break;
  case Board::FLAG:
    return 'X';
    break;
  }
}

void UI::drawBoard() {
  // get current board and print it in the window
  for (size_t ylocation = 0; ylocation < dimension_y_; ylocation++) {
    // collect line as string
    for (size_t xlocation = 0; xlocation < dimension_x_; xlocation++) {
      // at start board is not initialized
      // initialization will happen on first click
      // until then just print all as covered
      if (board_->isInitialized()) {
        // get type at given location
        size_t inidicator_value = 0;
        auto type = board_->getTypeAt(xlocation, ylocation, &inidicator_value);
        // set colorscheme based type
        setColor(type);
        // get symbol representation of type
        auto symbol = typeToSymbol(type);
        // if type == INDICATOR add indicator_value to ascii 0 (0x30)
        if (type == Board::INDICATOR)
          symbol += inidicator_value;
        // place symbol in window
        mvwaddch(game_window_, ylocation + 1, xlocation + 1, symbol);
      } else {
        setColor(Board::COVERED);
        mvwaddch(game_window_, ylocation + 1, xlocation + 1, '#');
      }
    }
  }
  // refresh window
  wrefresh(game_window_);
}

void UI::drawLegend() {

  mvwaddstr(legend_window, 1, 1, "Up:          k");
  mvwaddstr(legend_window, 2, 1, "Down:        j");
  mvwaddstr(legend_window, 3, 1, "Left:        h");
  mvwaddstr(legend_window, 4, 1, "Right:       l");
  mvwaddstr(legend_window, 6, 1, "Flag:        f");
  mvwaddstr(legend_window, 7, 1, "Uncover: space");
  mvwaddstr(legend_window, 8, 1, "Quit:        q");

  wrefresh(legend_window);
}

void UI::drawInfo() {

  size_t mines = board_->getMines();
  size_t flags = board_->getFlags();
  int remaining = int(mines) - int(flags);

  std::stringstream mines_line, flags_line, remaining_line;
  mines_line << "Mines:" << std::setw(10) << board_->getMines();
  flags_line << "Flags:" << std::setw(10) << board_->getFlags();
  remaining_line << "Remaining:" << std::setw(6) << remaining;

  mvwaddstr(information_window, 1, 1, mines_line.str().c_str());
  mvwaddstr(information_window, 2, 1, flags_line.str().c_str());
  mvwaddstr(information_window, 3, 1, remaining_line.str().c_str());

  wrefresh(information_window);
  wmove(game_window_, cursor_y_, cursor_x_);
}

void UI::initialize() {
  // Initialise Colorpairs (first = textcolor, second = bgcolor)
  init_pair(DEFAULT, COLOR_BLACK, COLOR_WHITE);
  init_pair(LOST, COLOR_WHITE, COLOR_RED);
  init_pair(WON, COLOR_WHITE, COLOR_BLUE);
  init_pair(MINE, COLOR_WHITE, COLOR_RED);
  init_pair(FLAG, COLOR_WHITE, COLOR_RED);
  init_pair(COVERED, COLOR_WHITE, COLOR_BLACK);
  init_pair(INDICATOR, COLOR_BLACK, COLOR_WHITE);

  initializeInformationWindow();
  initializeLegendWindow();
  initializeGameWindow();
}

void UI::initializeGameWindow() {
  // window width is dimension + 2 for a border in each direction
  size_t window_width = dimension_x_ + 2;
  size_t window_height = dimension_y_ + 2;

  size_t y_position = 0;
  if ((info_height + legend_height) > dimension_y_) {
    y_position = ((info_height + legend_height) - dimension_y_) / 2;
    // decrease by one to get index
    if (y_position > 0)
      y_position--;
  }
  // create game window next to info and legend window
  game_window_ =
      newwin(window_height, window_width, y_position, info_and_legend_width);
  // set default colorscheme
  wbkgd(game_window_, COLOR_PAIR(DEFAULT));
  // create border around window
  wborder(game_window_, '|', '|', '-', '-', '+', '+', '+', '+');
}

void UI::initializeInformationWindow() {
  // create information window in upper left corner
  information_window = newwin(info_height, info_and_legend_width, 0, 0);
  // create border around window
  wborder(information_window, '|', '|', '-', '-', '+', '+', '+', '+');
  drawInfo();
}

void UI::initializeLegendWindow() {

  // create information window in upper left corner
  legend_window = newwin(legend_height, info_and_legend_width, info_height, 0);
  // create border around window
  wborder(legend_window, '|', '|', '-', '-', '+', '+', '+', '+');

  drawLegend();
}
