#include "game.h"
#include <cmath>
#include <iostream>
#include <sstream>

Game::Game(size_t rows, size_t columns, size_t percentage)
    : rows_(rows), columns_(columns), percentage_(percentage) {}

Game::Game(Game::GAME_TYPE type) {

  switch (type) {
  case BEGINNER:
    rows_ = 9;
    columns_ = 9;
    percentage_ = 12;
    break;
  case INTERMEDIATE:
    rows_ = 16;
    columns_ = 16;
    percentage_ = 16;
    break;
  case EXPERT:
    rows_ = 20;
    columns_ = 20;
    percentage_ = 21;
    break;
  }
}

std::string Game::getLastError() { return last_error_; }

bool Game::isValid() {

  if (percentage_ < MINIMUM_PERCENTAGE) {
    setError("percentage of mines is below the minimum");
    return false;
  }

  if (percentage_ > MAXIMUM_PERCENTAGE) {
    setError("percentage of mines is above the maximum");
    return false;
  }

  if (getMineCount() == 0) {
    setError(
        "current percentage of mines does not even generate a single mine.");
    return false;
  }

  if (size_t(LINES) < (rows_ + 2) || size_t(COLS) < (columns_ + 2)) {
    setError("not enough space for this game in terminal");
  }

  return true;
}

size_t Game::getMineCount() {
  double multiplier = double(percentage_) / 100;
  size_t minecount = std::round(double(getFieldCount()) * multiplier);
  return minecount;
}

size_t Game::getFieldCount() { return (rows_ * columns_); }

bool Game::generateGameBoard() {
  if (!isValid())
    return false;
  game_board_ = std::make_shared<Board>(rows_, columns_, getMineCount());
  return true;
}

void Game::processKeyPress(char keypressed, size_t x, size_t y) {

  static bool finale_state_reached = false;

  switch (keypressed) {
  case 'q':
    userinterface_->terminate();
    break;
  case 'h':
    if (!finale_state_reached)
      userinterface_->left();
    break;
  case 'j':
    if (!finale_state_reached)
      userinterface_->down();
    break;
  case 'k':
    if (!finale_state_reached)
      userinterface_->up();
    break;
  case 'l':
    if (!finale_state_reached)
      userinterface_->right();
    break;
  case ' ':
    if (!finale_state_reached) {
      // check if it is first click
      // if it is -> initialize board
      // else uncover field
      if (!game_board_->isInitialized()) {
        game_board_->initializeBoard(x, y);
      } else
        game_board_->uncoverField(x, y);

      // check if finale state has been reached
      if (game_board_->hasBeenBlown()){
        userinterface_->setColorScheme(UI::LOST);
        finale_state_reached = true;
      }
      else if (game_board_->hasBeenWon()){
        userinterface_->setColorScheme(UI::WON);
        finale_state_reached = true;
      }
    }
    break;
  case 'f':
    if (!finale_state_reached && game_board_->isInitialized()) {
      game_board_->toggleFlag(x, y);
    }
  default:
    break;
  }
}

#include <unistd.h>
bool Game::startGame() {
  if (!generateGameBoard())
    return false;
  userinterface_ = std::make_shared<UI>(game_board_, columns_, rows_);

  if (!userinterface_->terminalSizeSufficient()) {
    setError("terminal to small");
    return false;
  }
  userinterface_->setKeyEventCallback(
      std::bind(&Game::processKeyPress, this, std::placeholders::_1,
                std::placeholders::_2, std::placeholders::_3));

  // Start displaying, will return when game finished
  userinterface_->run();

  return true;
}

void Game::setError(std::string msg) { last_error_ = msg; }
