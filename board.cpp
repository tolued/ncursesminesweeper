#include "board.h"
#include <algorithm>
#include <random>
#include <sstream>

Board::Board(size_t rows, size_t columns, size_t mines)
    : rows_(rows), columns_(columns), mines_(mines) {}

Board::FIELDTYPE Board::getTypeAt(size_t x, size_t y, size_t *indicator_value) {
  auto index = convertCoordinatesToIndex(x, y);
  if (board_[index].isCovered()) {
    if (board_[index].isFlagged())
      return FIELDTYPE::FLAG;
    return FIELDTYPE::COVERED;
  } else {
    if (board_[index].isMine())
      return FIELDTYPE::MINE;
    else if (board_[index].adjacentMines() == 0)
      return FIELDTYPE::BLANK;
    else {
      *indicator_value = board_[index].adjacentMines();
      return FIELDTYPE::INDICATOR;
    }
  }
}

bool Board::toggleFlag(size_t x, size_t y) {
  auto index = convertCoordinatesToIndex(x, y);
  // check if field is covered. If not return false, no flag can be set here
  if (!board_[index].isCovered())
    return false;
  board_[index].toggleFlag();
  return true;
}

bool Board::uncoverField(size_t x, size_t y) {
  auto index = convertCoordinatesToIndex(x, y);
  Field &field = board_[index];

  if (!field.isCovered()) {
    // if field is blank field uncover any adjacent field.
    if (field.adjacentMines() == 0)
      performClearAroundBlankField(field);
    else
      performClearAroundIndicatorField(field);
    return false;
  } else {
    // uncoverring flagged fields is not allowed
    if (field.isFlagged())
      return false;

    // check if it hit a mine. If so set blown flag
    if (field.isMine())
      blown_ = true;

    // uncover field as required.
    field.uncover();

    // if we hit an blank field keep clearing until no more adjacent empty files
    // found
    if (field.adjacentMines() == 0 && !field.isMine())
      performClearAroundBlankField(field);

    return true;
  }
}

bool Board::isInitialized() { return initialized_; }

bool Board::hasBeenBlown() { return blown_; }

bool Board::hasBeenWon() {
  // shortcut if game has already been verified to be won or if it is already
  // lost
  if (blown_)
    return false;
  if (won_)
    return true;

  size_t covered_fields = 0;
  // count remaining covered fields.
  // if covered = minecount we can be sure that the game has been won
  for (auto field : board_)
    if (field.isCovered())
      covered_fields++;
  if (covered_fields == mines_)
    won_ = true;
  return won_;
}

void Board::distributeMines(std::vector<size_t> disallowed_positions) {
  size_t range = rows_ * columns_;

  // create locations vector and set capacity
  std::vector<size_t> available_locations;
  available_locations.reserve(range);

  // fill vector with increasing location ids
  // each location id in disallowed position will be skipped
  for (size_t loop_counter = 0; loop_counter < range; loop_counter++) {
    auto location = std::find(disallowed_positions.begin(),
                              disallowed_positions.end(), loop_counter);
    if (location != disallowed_positions.end())
      continue;
    available_locations.push_back(loop_counter);
  }

  // create basic random generator
  // no need for fancy
  std::random_device random_seed;
  std::default_random_engine generator(random_seed());

  // decrease to generate random index and not count
  range--;
  for (size_t mines = mines_; mines > 0; mines--) {
    if (range == 0) // paranoid check
      break;
    // re-set range to remaining available locations
    // could also be just decreased, but to make sure later changes
    // can not corrupt this. This implementation should be a bit more safe
    range = available_locations.size() - 1;
    // create distribution. Range size will be decrease for each mine set
    std::uniform_int_distribution<size_t> distribution(0, range);

    // get next location index
    size_t locationindex = distribution(generator);
    auto location_iterator = available_locations.begin() + locationindex;

    size_t minelocation = *location_iterator;

    // delete location from remaining availables
    available_locations.erase(location_iterator);

    // set field on new location to mine
    board_[minelocation].placeMine();
  }
}

std::vector<size_t> Board::getAdjacentLocations(size_t center) {
  std::vector<size_t> locations;

  // collect information on own location on board
  bool hasLeftColumn = (center % columns_) > 0;
  bool hasRightColumn = (center % columns_) < (columns_ - 1);
  bool hasTopRow = ((center / columns_) % rows_) > 0;
  bool hasBottomRow = ((center / columns_) % rows_) < (rows_ - 1);

  // add all straight available directions
  if (hasLeftColumn)
    locations.push_back(center - 1);
  if (hasRightColumn)
    locations.push_back(center + 1);
  if (hasTopRow)
    locations.push_back(center - columns_);
  if (hasBottomRow)
    locations.push_back(center + columns_);

  // add upper left and upper right if available
  if (hasLeftColumn && hasTopRow)
    locations.push_back(center - columns_ - 1);
  if (hasRightColumn && hasTopRow)
    locations.push_back(center - columns_ + 1);

  // add bottom left and bottom right if available
  if (hasLeftColumn && hasBottomRow)
    locations.push_back(center + columns_ - 1);
  if (hasRightColumn && hasBottomRow)
    locations.push_back(center + columns_ + 1);

  return locations;
}

void Board::workOnAdjacentFields(size_t center,
                                 std::function<bool(Field &)> workfunction) {
  auto adjacent_locations = getAdjacentLocations(center);

  for (auto adjacent_location : adjacent_locations)
    if (workfunction(board_[adjacent_location]))
      break;
}

size_t Board::convertCoordinatesToIndex(size_t x, size_t y) {
  return (y * columns_) + x;
}

void Board::performClearAroundBlankField(Field &field) {
  auto index = convertCoordinatesToIndex(field.x(), field.y());

  workOnAdjacentFields(index, [&](Field &adjacent_field) {
    // if field already uncovered proceed to next
    if (adjacent_field.isFlagged() || !adjacent_field.isCovered())
      return false;
    uncoverField(adjacent_field.x(), adjacent_field.y());
    return false;
  });
}

void Board::performClearAroundIndicatorField(Field &field) {
  auto index = convertCoordinatesToIndex(field.x(), field.y());

  size_t flagcount = 0;
  // count adjacent flags
  workOnAdjacentFields(index, [&](Field &adjacent_field) {
    if (adjacent_field.isFlagged())
      flagcount++;
    return false;
  });

  // if flagcount != minecount return. Clear is not possible here
  if (flagcount != field.adjacentMines())
    return;

  workOnAdjacentFields(index, [&](Field &adjacent_field) {
    // if field is flagge or already uncoverred nothing has to be done here
    if (adjacent_field.isFlagged() || !adjacent_field.isCovered())
      return false;
    uncoverField(adjacent_field.x(), adjacent_field.y());
    return false;
  });
}

size_t Board::getMines() const
{
    return mines_;
}

size_t Board::getFlags()
{
    size_t flagcount=0;
    for (size_t location = 0; location < board_.size(); location++) {
        Field &field = board_[location];
     if(field.isFlagged() && field.isCovered())
         flagcount++;
    }
    return flagcount;

}

void Board::distributeIndicators() {
    for (size_t location = 0; location < board_.size(); location++) {
    // search for all field that are not containing a mine and calculate the
    // amount of mines around
    if (!board_[location].isMine()) {
      auto count = 0;
      // count the amount of adjacent mines
      workOnAdjacentFields(location, [&count](Field &field) {
        if (field.isMine())
          count++;
        return false;
      });

      board_[location].setAdjacentMineCount(count);
    }
  }
}

void Board::initializeBoard(size_t click_x, size_t click_y) {
  // Initialies empty board
  size_t range = columns_ * rows_;
  board_.reserve(range);

  size_t x = 0, y = 0;
  // Create Fields on board and teach them their own location
  while (range--) {
    board_.push_back(Field(x, y));
    x++;
    if (x == (columns_)) {
      y++;
      x = 0;
    }
  }

  auto index = convertCoordinatesToIndex(click_x, click_y);
  std::vector<size_t> disallowed_locations = getAdjacentLocations(index);
  disallowed_locations.push_back(index);

  // place all mines on random locations
  distributeMines(disallowed_locations);
  // calculate and set indicators
  distributeIndicators();
  // uncover field of first click
  uncoverField(click_x, click_y);
  // set flag that this board has been successfully initialized
  initialized_ = true;
}

size_t Field::x() const { return x_; }

size_t Field::y() const { return y_; }
