#ifndef BOARD_H
#define BOARD_H
#include <functional>
#include <string>
#include <vector>

class Field {
public:
  Field(size_t x, size_t y) : x_(x), y_(y) {}
  /** @brief Check if field is still covered
   * @return returns covered state
   */
  bool isCovered() { return covered_; }
  /** @brief Check if flag has been placed on field
   * @return returns if flag has been placed
   */
  bool isFlagged() { return flagged_; }
  /** @brief Check is field contains a mine
   * @return returns if mine is placed on field
   */
  bool isMine() { return ismine_; }
  /** @brief Get amount of adjacent mines.
   * @return count of adjacent mines. Will return 0 if field itself contains
   * mine
   */
  size_t adjacentMines() { return adjacent_mines_; }

  /** @brief place mine on field
   */
  void placeMine() { ismine_ = true; }
  /**
   * @brief toggle flag status
   */
  void toggleFlag() { flagged_ = !flagged_; }
  /**
   * @brief uncover field
   */
  void uncover() { covered_ = false; }
  /**
   * @brief setAdjacentMineCount to given amount
   * @param count amount of mined adjacent to this field
   */
  void setAdjacentMineCount(size_t count) { adjacent_mines_ = count; }

  size_t x() const;
  size_t y() const;

private:
  bool covered_ = true;
  bool flagged_ = false;
  bool ismine_ = false;
  size_t adjacent_mines_ = 0, x_ = 0, y_ = 0;
};

class Board {
public:
  typedef enum FIELDTYPE { MINE, FLAG, INDICATOR, BLANK, COVERED } FIELDTYPE;
  Board(size_t rows, size_t columns, size_t mines);

  /**
   * @brief getTypeAt Get field type at given location
   * @param x x-coordinate of the field on the board
   * @param y y-coordinate of the field on the board
   * @param indicator_value optional parameter. If pointer != nullptr and the
   * field is type INDICATOR the indicator value will be set on the addr
   * @return returns type of the field
   */
  FIELDTYPE getTypeAt(size_t x, size_t y, size_t *indicator_value);

  /**
   * @brief take all steps to initialize board.
   * @param click_x location on x-axis wher first click has been placed
   * @param click_y location on y-axis wher first click has been placed
   */
  void initializeBoard(size_t click_x, size_t click_y);

  /**
   * @brief toggleFlag Place or remove flag on given field if possible
   * @param x position of x-axis where flag should be placed
   * @param y position of y-axis where flag should be placed
   * @return returns true if flag could be set. Fails if field is already
   * uncovered.
   */
  bool toggleFlag(size_t x, size_t y);

  /**
   * @brief uncoverField uncover a field. If field is already uncovered
   * and amount of flags adjacent to given field is equals to the indicator,
   * force uncover any other adjacent field.
   * @param x position of x-axis where field should be uncovered
   * @param y position of y-axis where field should be uncovered
   * @return returns true if field could be uncovred. Return value does
   * not indicate if mine has been hit. For this information check status
   * after every change.
   */
  bool uncoverField(size_t x, size_t y);

  /**
   * @brief isInitialised check if board has already been initialized.
   * @return true if init has been executed
   */
  bool isInitialized();

  /**
   * @brief hasBeenBlown Check if mine has been blown
   * @return true if mine has been hit
   */
  bool hasBeenBlown();

  /**
   * @brief hasBeenWon check if game has been won
   * @return true if game won
   */
  bool hasBeenWon();

  size_t getMines() const;
  size_t getFlags() ;

private:
  /**
   * @brief distributeMines distribute required mines on board
   * @param disallowed_positions list of positions on which mines are not
   * allowed. this is required to make sure the first click is not a mine
   */
  void distributeMines(std::vector<size_t> disallowed_positions);

  /**
   * @brief distributeIndicators for each field not containing a mine
   */
  void distributeIndicators();

  /**
   * @brief getAdjacentLocations get a vector of all idx that are adjacent to
   * the current field
   * @param center the current field to which the adjacent files should be
   * searched
   * @return returns a vector containing all id's on board that are adjacent
   */
  std::vector<size_t> getAdjacentLocations(size_t center);

  /**
   * @brief workOnAdjacentFields allows to work on adjacent fields of a given
   * index
   * @param center index of field on which adjacent field the given function
   * should be applied
   * @param workfunction function will be called with every adjacent field. If
   * return value == true processing will be stopped!
   */
  void workOnAdjacentFields(size_t center,
                            std::function<bool(Field &field)> workfunction);

  /**
   * @brief convertCoordinatesToIndex convert given x and y value to a valid
   * index in the data structure
   * @param x the x position
   * @param y the y position
   * @return returns valid index on board
   */
  size_t convertCoordinatesToIndex(size_t x, size_t y);

  /**
   * @brief performClearAroundBlankField Clear all fields adjacent to a blank
   * field field. If it hits an blank field adjacent, this action will also
   * performed for that field.
   * @param field Field the clear should be performed around
   */
  void performClearAroundBlankField(Field &field);

  /**
   * @brief performClearAroundIndicatorField Perform a clear on adjacent fields
   * around an indicator field
   * @param field field the clear should be performed around
   */
  void performClearAroundIndicatorField(Field &field);

  std::vector<Field> board_;
  size_t rows_ = 0, columns_ = 0, mines_ = 0;
  bool blown_ = false, initialized_ = false, won_ = false;
};

#endif
