#include "board.h"
#include "game.h"
#include <iostream>
#include <ncurses.h>

void printHelp() {
  std::cout << "ncurses-minesweeper <MODE>" << std::endl;
  std::cout << std::endl;
  std::cout << "MODES" << std::endl;
  std::cout << "\t[b]eginner" << std::endl;
  std::cout << "\t[i]ntermediate" << std::endl;
  std::cout << "\t[e]xpert" << std::endl;
}
int main(int argc, char *argv[]) {

  auto type = Game::BEGINNER;

  if (argc == 1) {
    printHelp();
    return 1;
  }
  std::string mode_string(argv[1]);

  //if mode string is longer than one character -> get first character
  if (mode_string.length() > 1)
    mode_string = mode_string.substr(0, 1);

  if (mode_string == "b" || mode_string == "B")
    type = Game::BEGINNER;
  if (mode_string == "i" || mode_string == "I")
    type = Game::INTERMEDIATE;
  if (mode_string == "e" || mode_string == "E")
    type = Game::EXPERT;

  Game game(type);

  if (!game.startGame()) {
    std::cout << "Could not start game: ";
    std::cout << game.getLastError() << std::endl;
    return 1;
  }

  return 0;
}
