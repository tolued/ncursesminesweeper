#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H
#include "board.h"
#include <functional>
#include <memory>
#include <ncurses.h>

class UI {
public:
  typedef enum COLORSCHEMES {
    DEFAULT = 1,
    LOST,
    WON,
    MINE,
    FLAG,
    COVERED,
    INDICATOR
  } COLORSCHEMES;
  typedef std::function<void(char, size_t, size_t)> EventCallback;

  UI(std::shared_ptr<Board> board, size_t dimension_x, size_t dimension_y);
  ~UI();
  /**
   * @brief setKeyEventCallback set event callback function which will receive
   * every key press
   * @param callback pointer to the callback function that has to process key
   * presses
   */
  void setKeyEventCallback(EventCallback callback);

  /**
   * @brief terminalSizeSufficient check if required game has enough space in
   * current terminal
   * @return returns true if terminal size fits the required game
   */
  bool terminalSizeSufficient();

  /**
   * @brief up move cursor up
   */
  void up();
  /**
   * @brief down move cursor down
   */
  void down();
  /**
   * @brief left move cursor left
   */
  void left();
  /**
   * @brief right move cursor right
   */
  void right();

  /**
   * @brief setColorScheme set color scheme for game window
   * @param scheme predefined color scheme
   */
  void setColorScheme(COLORSCHEMES scheme);

  /**
   * @brief run start displaying until terminate is set
   */
  void run();

  /**
   * @brief terminate request ui to stop displaying
   */
  void terminate();

private:
  /**
   * @brief setColor Switch color pair for characters based on their type
   * @param type type of the field that will be printed
   */
  void setColor(Board::FIELDTYPE type);
  /**
   * @brief typeToSymbol Convert fieldtype to a character representation in the
   * ui
   * @param type type of the field
   * @return character representation of the field
   */
  char typeToSymbol(Board::FIELDTYPE type);
  /**
   * @brief drawBoard redraw board
   */
  void drawBoard();
  void drawLegend();
  void drawInfo();
  /**
   * @brief initialize initialize windows
   */
  void initialize();
  void initializeGameWindow();
  void initializeInformationWindow();
  void initializeLegendWindow();

  //Windows
  WINDOW *game_window_ = nullptr;
  WINDOW *information_window = nullptr;
  WINDOW *legend_window = nullptr;

  //Basic board information and datastructure
  std::shared_ptr<Board> board_;
  size_t dimension_x_, dimension_y_, cursor_x_ = 1, cursor_y_ = 1;
  bool terminate_ = false;

  //Never changing constants
  static const size_t info_and_legend_width = 18;
  static const size_t info_height = 5;
  static const size_t legend_height = 10;

  //registered callback functions
  EventCallback event_callback_;
};

#endif // USER_INTERFACE_H
