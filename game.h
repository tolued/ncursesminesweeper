#ifndef GAME_H
#define GAME_H

#include <cstddef>
#include <stdint.h>
#include <string>
#include <chrono>
#include <memory>
#include "board.h"
#include "user_interface.h"

class Game {

public:
  typedef enum GAME_TYPE {
    BEGINNER,
    INTERMEDIATE,
    EXPERT
  } GAME_TYPE; // classic difficulties
  static const size_t MINIMUM_PERCENTAGE =
      1; // minimum percentage allowed for custom fields
  static const size_t MAXIMUM_PERCENTAGE =
      90; // maximum percentage allowed for custom fields

  /**
   * @brief Create a custom minefield
   * @param rows count of rows in game
   * @param columns count of columns in game
   * @param percentage of mines. Valid values are between MINIMUM_PERCENTAGE and
   * MAXIMUM_PERCENTAGE. Default is 16%.
   */
  Game(size_t rows, size_t columns, size_t percentage = 16);
  /**
   * @brief Create a classic minefield
   * @param type Select a Game::GAME_TYPE to play
   */
  Game(GAME_TYPE type);
  /**
   * @brief read last error
   * @return string of last error
   */
  std::string getLastError();
  /**
   * @brief check if a configuration is valid
   * @param error if is not nullptr will contain reasons why configuration is not valid
   * if check fails
   * @return returns true if configuration is valid and false if not
   */
  bool isValid();
  /**
   * @brief get the total amount of mines that will be generate
   * @return amount of mines that will be generated
   */
  size_t getMineCount();
  /**
   * @brief get the total amount of field in the game
   * @return amount of fields that will be generated
   */
  size_t getFieldCount();

  /**
   * @brief generate game board if configuration is valid
   * @param click_x position on x-axis of first click to make sure first click is not a mine
   * @param click_y position of y-axis first click to make sure first click is not a mine
   * @return returns false if generation failed because of invalid configuration
   */
  bool generateGameBoard();

  /**
   * @brief processKeyPress
   * @param keypressed the key that has been pressed
   */
  /**
   * @brief processKeyPress will process key presses and execute action on board as required
   * @param keypresse the key that has been pressed
   * @param x current x-axis location of cursor
   * @param y current y-axis location of cursor
   */
  void processKeyPress(char keypressed, size_t x, size_t y);

  /**
   * @brief startGame will start the initialized game. Will return when game is finished
   */
  bool startGame();

private:
  size_t rows_ = 0, columns_ = 0, percentage_ = 0;
  std::string last_error_ = "";
  std::shared_ptr<Board> game_board_ = nullptr;
  std::shared_ptr<UI> userinterface_ = nullptr;

  void setError(std::string msg);
};

#endif
